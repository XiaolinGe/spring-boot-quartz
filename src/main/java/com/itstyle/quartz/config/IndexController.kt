package com.itstyle.quartz.config

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping

/**
 * 通用访问拦截匹配
 * 创建者 科帮网
 * 创建时间	2018年4月3日
 */
@Controller
class IndexController {

    /**
     * 页面跳转
     * @param module
     * @param url
     * @return
     */
    @RequestMapping("{url}.shtml")
    fun page(@PathVariable("url") url: String): String {
        return url
    }

    /**
     * 页面跳转(二级目录)
     * @param module
     * @param function
     * @param url
     * @return
     */
    @RequestMapping("{module}/{url}.shtml")
    fun page(@PathVariable("module") module: String, @PathVariable("url") url: String): String {
        return "$module/$url"
    }

}
