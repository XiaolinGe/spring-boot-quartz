package com.itstyle.quartz

import org.quartz.SchedulerException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

/**
 * 启动类
 * 创建者 科帮网
 * 创建时间	2018年3月28日
 * API接口测试：http://localhost:8080/task/swagger-ui.html
 */
@SpringBootApplication
open class Application {
    private val logger = LoggerFactory.getLogger(Application::class.java!!)



}

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}