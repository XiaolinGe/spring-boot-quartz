package com.itstyle.quartz.job

import com.itstyle.quartz.service.IJobService
import org.quartz.Job
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException
import org.springframework.beans.factory.annotation.Autowired

import java.io.Serializable

/**
 * 实现序列化接口、防止重启应用出现quartz Couldn't retrieve job because a required class was not found 的问题
 */
class TestJob2 : Job, Serializable {

    @Autowired
    private val jobService: IJobService? = null

    @Throws(JobExecutionException::class)
    override fun execute(context: JobExecutionContext) {
        println(111)
        //payeeservice.xxxx()


    }

    companion object {

        private const val serialVersionUID = 1L
    }
}
