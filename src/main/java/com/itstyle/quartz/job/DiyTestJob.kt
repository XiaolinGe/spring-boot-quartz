package com.itstyle.quartz.job

import org.quartz.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.io.Serializable
import java.util.Date

/**
 * Job 的实例要到该执行它们的时候才会实例化出来。每次 Job 被执行，一个新的 Job 实例会被创建。
 * 其中暗含的意思就是你的 Job 不必担心线程安全性，因为同一时刻仅有一个线程去执行给定 Job 类的实例，甚至是并发执行同一 Job 也是如此。
 */
class DiyTestJob : Job, Serializable {

    @Throws(JobExecutionException::class)
    override fun execute(context: JobExecutionContext) {
        println("DIY Test-01-测试集群模式")

        println("Hello! DIY Test is executing." + Date())
        //取得job详情
        val jobDetail = context.jobDetail
        // 取得job名称
        val jobName = jobDetail.javaClass.getName()
        logger.info("Name: " + jobDetail.javaClass.getSimpleName())
        //取得job的类
        logger.info("Job Class: " + jobDetail.jobClass)
        //取得job开始时间
        logger.info(jobName + " fired at " + context.fireTime)
        //取得job下次触发时间
        logger.info("Next fire time " + context.nextFireTime)

        val dataMap = jobDetail.jobDataMap

        logger.info("itstyle: " + dataMap.getString("itstyle"))
        logger.info("blog: " + dataMap.getString("blog"))


    }

    companion object {

        private val logger = LoggerFactory.getLogger(DiyTestJob::class.java!!)
        private const val serialVersionUID = 1L
    }
}
