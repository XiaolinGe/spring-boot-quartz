package com.itstyle.quartz.dynamicquery

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext
import javax.persistence.Query

import org.hibernate.SQLQuery
import org.hibernate.transform.Transformers
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Repository

/**
 * 动态jpql/nativesql查询的实现类
 * 创建者 张志朋
 * 创建时间	2018年3月8日
 */
@Repository
class DynamicQueryImpl : DynamicQuery {

    internal var logger = LoggerFactory.getLogger(DynamicQueryImpl::class.java!!)

    @PersistenceContext
    val entityManager: EntityManager? = null

    override fun save(entity: Any) {
        entityManager!!.persist(entity)
    }

    override fun update(entity: Any) {
        entityManager!!.merge(entity)
    }

    override fun <T> delete(entityClass: Class<T>, entityid: Any) {
        delete(entityClass, arrayOf(entityid))
    }

    override fun <T> delete(entityClass: Class<T>, entityids: Array<Any>) {
        for (id in entityids) {
            entityManager!!.remove(entityManager.getReference(entityClass, id))
        }
    }

    private fun createNativeQuery(sql: String, vararg params: Any): Query {
        val q = entityManager!!.createNativeQuery(sql)
        if (params != null && params.size > 0) {
            for (i in params.indices) {
                q.setParameter(i + 1, params[i]) // 与Hiberante不同,jpa
                // query从位置1开始
            }
        }
        return q
    }

    override fun <T> nativeQueryList(nativeSql: String, vararg params: Any): List<T> {
        val q = createNativeQuery(nativeSql, *params)
        //@todo develop a new approach to result transformers
        q.unwrap(SQLQuery::class.java).setResultTransformer(Transformers.TO_LIST)
        return q.resultList as List<T>
    }

    override fun <T> nativeQueryListModel(resultClass: Class<T>,
                                          nativeSql: String, vararg params: Any): List<T> {
        val q = createNativeQuery(nativeSql, *params)
        //@todo develop a new approach to result transformers
        q.unwrap<SQLQuery<*>>(SQLQuery::class.java).setResultTransformer(Transformers.aliasToBean(resultClass))
        return q.resultList as List<T>
    }

    override fun <T> nativeQueryListMap(nativeSql: String, vararg params: Any): List<T> {
        val q = createNativeQuery(nativeSql, *params)
        //@todo develop a new approach to result transformers
        q.unwrap<SQLQuery<*>>(SQLQuery::class.java).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
        return q.resultList as List<T>
    }

    override fun nativeQueryCount(nativeSql: String, vararg params: Any): Long? {
        val count = createNativeQuery(nativeSql, *params).singleResult
        return (count as Number).toLong()
    }

}
