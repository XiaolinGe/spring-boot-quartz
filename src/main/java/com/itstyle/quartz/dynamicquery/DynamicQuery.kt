package com.itstyle.quartz.dynamicquery

/**
 * 扩展SpringDataJpa, 支持动态jpql/nativesql查询并支持分页查询
 * 使用方法：注入ServiceImpl
 * 创建者 张志朋
 * 创建时间	2018年3月8日
 */
interface DynamicQuery {

    fun save(entity: Any)

    fun update(entity: Any)

    fun <T> delete(entityClass: Class<T>, entityid: Any)

    fun <T> delete(entityClass: Class<T>, entityids: Array<Any>)

    /**
     * 查询对象列表，返回List
     * @param resultClass
     * @param nativeSql
     * @param params
     * @return  List<T>
     * @Date    2018年3月15日
     * 更新日志
     * 2018年3月15日  张志朋  首次创建
    </T> */
    fun <T> nativeQueryList(nativeSql: String, vararg params: Any): List<T>

    /**
     * 查询对象列表，返回List<Map></Map><key></key>,value>>
     * @param nativeSql
     * @param params
     * @return  List<T>
     * @Date    2018年3月15日
     * 更新日志
     * 2018年3月15日  张志朋  首次创建
    </T> */
    fun <T> nativeQueryListMap(nativeSql: String, vararg params: Any): List<T>

    /**
     * 查询对象列表，返回List<组合对象>
     * @param resultClass
     * @param nativeSql
     * @param params
     * @return  List<T>
     * @Date    2018年3月15日
     * 更新日志
     * 2018年3月15日  张志朋  首次创建
    </T></组合对象> */
    fun <T> nativeQueryListModel(resultClass: Class<T>, nativeSql: String, vararg params: Any): List<T>

    /**
     * 执行nativeSql统计查询
     * @param nativeSql
     * @param params 占位符参数(例如?1)绑定的参数值
     * @return 统计条数
     */
    fun nativeQueryCount(nativeSql: String, vararg params: Any): Long?


}
