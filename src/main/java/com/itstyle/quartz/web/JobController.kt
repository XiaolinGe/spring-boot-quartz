package com.itstyle.quartz.web


import javax.servlet.http.HttpServletResponse

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import com.itstyle.quartz.entity.QuartzEntity
import com.itstyle.quartz.entity.Result
import com.itstyle.quartz.service.IJobService
import org.quartz.*

@RestController
@RequestMapping("/job")
class JobController {


    @Autowired
    private val scheduler: Scheduler? = null
    @Autowired
    private val jobService: IJobService? = null

    @PostMapping("/add")
    fun save(quartz: QuartzEntity): Result {
        LOGGER.info("新增任务")
        try {
            //如果是修改  展示旧的 任务
            if (quartz.oldJobGroup != null) {
                val key = JobKey(quartz.oldJobName!!, quartz.oldJobGroup)
                scheduler!!.deleteJob(key)
            }
            val cls = Class.forName(quartz.jobClassName)
            cls.newInstance()
            //构建job信息
            val job = JobBuilder.newJob(cls as Class<out Job>?).withIdentity(quartz.jobName!!,
                    quartz.jobGroup)
                    .withDescription(quartz.description).build()
            // 触发时间点
            val cronScheduleBuilder = CronScheduleBuilder.cronSchedule(quartz.cronExpression!!)
            val trigger = TriggerBuilder.newTrigger().withIdentity("trigger" + quartz.jobName!!, quartz.jobGroup)
                    .startNow().withSchedule<CronTrigger>(cronScheduleBuilder).build()
            //交由Scheduler安排触发
            scheduler!!.scheduleJob(job, trigger)
        } catch (e: Exception) {
            e.printStackTrace()
            return Result.error()
        }

        return Result.ok()
    }

    @PostMapping("/list")
    fun list(quartz: QuartzEntity, pageNo: Int?, pageSize: Int?): Result {
        LOGGER.info("任务列表")
        val list = jobService!!.listQuartzEntity(quartz, pageNo, pageSize)
        return Result.ok(list)
    }

    @PostMapping("/trigger")
    fun trigger(quartz: QuartzEntity, response: HttpServletResponse): Result {
        LOGGER.info("触发任务")
        try {
            val key = JobKey(quartz.jobName!!, quartz.jobGroup)
            scheduler!!.triggerJob(key)
        } catch (e: SchedulerException) {
            e.printStackTrace()
            return Result.error()
        }

        return Result.ok()
    }

    @PostMapping("/pause")
    fun pause(quartz: QuartzEntity, response: HttpServletResponse): Result {
        LOGGER.info("停止任务")
        try {
            val key = JobKey(quartz.jobName!!, quartz.jobGroup)
            scheduler!!.pauseJob(key)
        } catch (e: SchedulerException) {
            e.printStackTrace()
            return Result.error()
        }

        return Result.ok()
    }

    @PostMapping("/resume")
    fun resume(quartz: QuartzEntity, response: HttpServletResponse): Result {
        LOGGER.info("恢复任务")
        try {
            val key = JobKey(quartz.jobName!!, quartz.jobGroup)
            scheduler!!.resumeJob(key)
        } catch (e: SchedulerException) {
            e.printStackTrace()
            return Result.error()
        }

        return Result.ok()
    }

    @PostMapping("/remove")
    fun remove(quartz: QuartzEntity, response: HttpServletResponse): Result {
        LOGGER.info("移除任务")
        try {
            val triggerKey = TriggerKey.triggerKey(quartz.jobName!!, quartz.jobGroup)
            // 停止触发器
            scheduler!!.pauseTrigger(triggerKey)
            // 移除触发器
            scheduler.unscheduleJob(triggerKey)
            // 删除任务
            scheduler.deleteJob(JobKey.jobKey(quartz.jobName!!, quartz.jobGroup))
            println("removeJob:" + JobKey.jobKey(quartz.jobName!!))
        } catch (e: Exception) {
            e.printStackTrace()
            return Result.error()
        }

        return Result.ok()
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(JobController::class.java!!)
    }
}
