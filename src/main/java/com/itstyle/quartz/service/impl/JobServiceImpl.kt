package com.itstyle.quartz.service.impl

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.util.StringUtils

import com.itstyle.quartz.dynamicquery.DynamicQuery
import com.itstyle.quartz.entity.QuartzEntity
import com.itstyle.quartz.service.IJobService

@Service("jobService")
class JobServiceImpl : IJobService {

    @Autowired
    private val dynamicQuery: DynamicQuery? = null

    override fun listQuartzEntity(quartz: QuartzEntity,
                                  pageNo: Int?, pageSize: Int?): List<QuartzEntity> {
        val nativeSql = StringBuffer()
        nativeSql.append("SELECT job.JOB_NAME as jobName,job.JOB_GROUP as jobGroup,job.DESCRIPTION as description,job.JOB_CLASS_NAME as jobClassName,")
        nativeSql.append("cron.CRON_EXPRESSION as cronExpression,tri.TRIGGER_NAME as triggerName,tri.TRIGGER_STATE as triggerState,")
        nativeSql.append("job.JOB_NAME as oldJobName,job.JOB_GROUP as oldJobGroup ")
        nativeSql.append("FROM qrtz_job_details AS job LEFT JOIN qrtz_triggers AS tri ON job.JOB_NAME = tri.JOB_NAME ")
        nativeSql.append("LEFT JOIN qrtz_cron_triggers AS cron ON cron.TRIGGER_NAME = tri.TRIGGER_NAME ")
        nativeSql.append("WHERE tri.TRIGGER_TYPE = 'CRON'")
        var params = arrayOf<Any>()
        if (!StringUtils.isEmpty(quartz.jobName)) {//加入JobName搜索其他条件自行实现
            nativeSql.append(" AND job.JOB_NAME = ?")
            params = arrayOf(quartz.jobName) as Array<Any>
        }
        return dynamicQuery!!.nativeQueryListModel(QuartzEntity::class.java, nativeSql.toString(), *params)
    }

    override fun listQuartzEntity(quartz: QuartzEntity): Long? {
        val nativeSql = StringBuffer()
        nativeSql.append("SELECT COUNT(*)")
        nativeSql.append("FROM qrtz_job_details AS job LEFT JOIN qrtz_triggers AS tri ON job.JOB_NAME = tri.JOB_NAME ")
        nativeSql.append("LEFT JOIN qrtz_cron_triggers AS cron ON cron.TRIGGER_NAME = tri.TRIGGER_NAME ")
        nativeSql.append("WHERE tri.TRIGGER_TYPE = 'CRON'")
        return dynamicQuery!!.nativeQueryCount(nativeSql.toString(), *arrayOf())
    }
}
