package com.itstyle.quartz.service

import com.itstyle.quartz.entity.QuartzEntity

interface IJobService {

    fun listQuartzEntity(quartz: QuartzEntity, pageNo: Int?, pageSize: Int?): List<QuartzEntity>

    fun listQuartzEntity(quartz: QuartzEntity): Long?
}
