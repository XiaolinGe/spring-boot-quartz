package com.itstyle.quartz.entity

/**
 * 任务类
 * 创建者	张志朋
 * 创建时间	2018年3月28日
 */
class QuartzEntity {

    var jobName: String? = null//任务名称
    var jobGroup: String? = null//任务分组
    var description: String? = null//任务描述
    var jobClassName: String? = null//执行类
    var cronExpression: String? = null//执行时间
    var triggerName: String? = null//执行时间
    var triggerState: String? = null//任务状态

    var oldJobName: String? = null//任务名称 用于修改
    var oldJobGroup: String? = null//任务分组 用于修改

    constructor() : super() {}
    constructor(jobName: String, jobGroup: String, description: String, jobClassName: String, cronExpression: String, triggerName: String) : super() {
        this.jobName = jobName
        this.jobGroup = jobGroup
        this.description = description
        this.jobClassName = jobClassName
        this.cronExpression = cronExpression
        this.triggerName = triggerName
    }
}
