package com.itstyle.quartz.entity

import java.util.HashMap

/**
 * 页面响应entity
 * 创建者 张志朋
 * 创建时间	2018年3月8日
 */
class Result : HashMap<String, Any>() {
    init {
        put("code", 0)
    }

    override fun put(key: String, value: Any): Result {
        super.put(key, value)
        return this
    }

    companion object {

        private val serialVersionUID = 1L

        fun error(msg: String): Result {
            return error(500, msg)
        }

        @JvmOverloads
        fun error(code: Int = 500, msg: String = "未知异常，请联系管理员"): Result {
            val r = Result()
            r["code"] = code
            r["msg"] = msg
            return r
        }

        fun ok(msg: Any): Result {
            val r = Result()
            r["msg"] = msg
            return r
        }


        fun ok(map: Map<String, Any>): Result {
            val r = Result()
            r.putAll(map)
            return r
        }

        fun ok(): Result {
            return Result()
        }
    }
}